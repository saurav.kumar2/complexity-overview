#include <iostream>
#include <vector>
using namespace std;

// create : add new data in the end
// read : read the random located data
// update : update the value of last accessible data
// delete : delete the last accessible data

template <typename T> // primitve data type
class Array{
    private:
        vector<T> arr;
        int calc_size(){
            return arr.size();
        }

        T get(int Nx){
            return arr[Nx];
        }

        T* access(int Nx){
            return &arr[Nx];
        }

        void erase(int Nx){
            arr.pop_back();
        }
        void print(){
            cout << "size: " << arr.size( ) << "\n";
            for(auto i:arr){
                cout << i << " ";
            }
            cout <<"\n";
        }

    public:
        Array (int N){
            arr.resize(N);
        }

        void create(){
            unsigned int size = calc_size();
            arr.resize(size + 1);
            arr[size] = (T)(5.0);
        }

        template<int Nx>
        T read(){
            unsigned int size = calc_size();
            return get(Nx);
        }

        template<int Nx>
        void update(){
            *access(Nx) = (T)(5.0);
            print();
        }

        void remove(){
            unsigned int size = calc_size();
            erase(size - 1);
            print();
        }
};

int main(){
    Array <int> m(10);
    m.create();
    m.read<5>();
    m.update<5>();
    m.remove();
    return 0;
}